package Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.BeforeClass;
import org.junit.Test;

import eCheque.ECheque;
import eCheque.EChequeIO;

public class EChequeIOTest

{

static ECheque SaveECheque;

static String eChequeFileName;
static String AccountHolderVariable = "Shashank";
static String BankVariable = "RBC";
static String AccountNoVariable	= "12345";
static String CurrencyVariable = "CAD";
static String MoneyAmountVariable = "$1000";
static String ChequeNoVariable = "1234567";
static String PayToOrderOfVariable = "Peter";
static String EarnDayVariable = "Monday";

static boolean GuaranteeVariable = true;

static byte[] BankSign	= {0x00, 0x7f};
static byte[] DrawerSign= {0x00, 0x7f};

@BeforeClass
public static void setUpBeforeClass() throws Exception 
{
	eChequeFileName = "filename";

SaveECheque = new ECheque();
SaveECheque.setaccountholder(AccountHolderVariable);
SaveECheque.setbankname(BankVariable);
SaveECheque.setaccountNumber(AccountNoVariable);
SaveECheque.setcurrencytype(CurrencyVariable);
SaveECheque.setamountofMony(MoneyAmountVariable);
SaveECheque.setchequeNumber(ChequeNoVariable);
SaveECheque.setpayToOrderOf(PayToOrderOfVariable);
SaveECheque.setearnday(EarnDayVariable);

SaveECheque.setguaranteed(GuaranteeVariable);
SaveECheque.setdrawersiganure(DrawerSign);

EChequeIO eChequeToSave=new EChequeIO();
eChequeToSave.savecheque(SaveECheque, eChequeFileName);

}

@Test
public void testsavecheque() throws IOException 
{
EChequeIO eChequeToSave=new EChequeIO();
eChequeToSave.savecheque(SaveECheque, eChequeFileName);
}

@Test
public void testReadcheque() throws ClassNotFoundException, IOException
{

EChequeIO eChequeToLoad=new EChequeIO();
ECheque eq;
eq=eChequeToLoad.readcheque(eChequeFileName);
		
if(eChequeToLoad != null)
{
assertEquals(AccountHolderVariable, eq.getaccountholder());
assertEquals(BankVariable, eq.getbankname());
assertEquals(AccountNoVariable, eq.getaccountNumber());
assertEquals(CurrencyVariable, eq.getcurrencytype());
assertEquals(MoneyAmountVariable, eq.getMoney());
assertEquals(ChequeNoVariable, eq.getchequeNumber());
assertEquals(PayToOrderOfVariable, eq.getpayToOrderOf());
assertEquals(EarnDayVariable, eq.getearnday());
assertEquals(GuaranteeVariable, eq.getguaranteed());
assertArrayEquals(DrawerSign, eq.getdrawersiganure());
}
}
}