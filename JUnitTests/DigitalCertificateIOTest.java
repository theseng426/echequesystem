package eCheque;

import static org.junit.Assert.*;
import java.security.PublicKey;
import java.io.File;
import java.security.PublicKey;
import java.io.IOException;
import java.security.KeyPair;
import org.junit.Test;
import org.junit.BeforeClass;


public class DigitalCertificateIOTest
{

static DigitalCertificate instanceCertificate;
static DigitalCertificateIO dc;
static String filePath;
static PublicKey PublicKey;
	
@BeforeClass
public static void setUpBeforeClass() throws Exception 
{
byte[] bytea = {3,6,11,15,19,25};
RSAGenerator keyGen = new RSAGenerator();
KeyPair RSAKeys = keyGen.GenerateRSAKeys();
dc=new DigitalCertificateIO();
PublicKey = RSAKeys.getPublic();
instanceCertificate = new DigitalCertificate();
filePath = new String("D:\\DCertificate.certificate");
instanceCertificate.setPublicKey(PublicKey);
instanceCertificate.setHolderName("Peter");
instanceCertificate.setIssuer("Shashank");
instanceCertificate.setSubject("DC");
instanceCertificate.setSerialNumber("12345");
instanceCertificate.setValidFrom("July 20, 2016");
instanceCertificate.setValidTo("July 30, 2016");
instanceCertificate.setIssuerSignature(bytea);
}

@Test
public void saveDCWithoutErrorTest() 
{
DigitalCertificate dcio = new DigitalCertificate();

try 
{
dc.SaveDC(dcio, filePath);
} 
	catch (IOException e) 
	{
	fail();
	}
		
File f = new File(filePath);
assertTrue(f.exists());
}
	
@Test
public void readDigitalCertificateCertExistsTest()
{
DigitalCertificate dcio = new DigitalCertificate();
DigitalCertificate localCert;
try 
{
dc.SaveDC(dcio, filePath);
}
	catch (IOException e)
	{
	fail("Error creating certificate");
	}
		
try
{
localCert = dc.readDigitalCertificate(filePath);
}
	catch (Exception e)
	{
	fail();
	return;
	}
	
assertEquals(localCert.getHolderName(), instanceCertificate.getHolderName());
assertEquals(localCert.getSubject(), instanceCertificate.getSubject());
assertEquals(localCert.getIssuer(), instanceCertificate.getIssuer());
assertEquals(localCert.getValidFrom(), instanceCertificate.getValidFrom());
assertEquals(localCert.getValidTo(), instanceCertificate.getValidTo());
assertEquals(localCert.getSerialNumber(), instanceCertificate.getSerialNumber());
assertEquals(localCert.getpublicKey(), instanceCertificate.getpublicKey());
assertArrayEquals(localCert.getIssuerSignature(), instanceCertificate.getIssuerSignature());
}
	
@Test
public void readNonExistantCertificateTest()
{
File f = new File(filePath);
f.delete();
		
try
{
dc.readDigitalCertificate(filePath);
}
	catch ( IOException e)
	{
	return;
	}
	catch (ClassNotFoundException e)
	{
	fail();
	return;
	}
	fail();
		
}

}